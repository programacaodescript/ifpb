#!/bin/bash

echo "Digite o primeiro número:"
read numero1

echo "Digite o segundo número:"
read numero2

soma=$((numero1 + numero2))

echo "A soma de $numero1 e $numero2 é igual a $soma."

produto=$((numero1 * numero2 +2))

echo "O produto do primeiro número ($numero1) pelo segundo número ($numero2), somado com 2, é igual a $produto."
exit 1
